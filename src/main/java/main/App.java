package main;

import org.apache.velocity.app.VelocityEngine;
import spark.ModelAndView;
import spark.template.velocity.VelocityTemplateEngine;

import java.math.BigDecimal;
import java.util.HashMap;

import static spark.Spark.get;

public class App {
    public static void main(String[] args) {
        VelocityEngine configuredEngine = new VelocityEngine();
        configuredEngine.setProperty("runtime.references.strict", true);
        configuredEngine.setProperty("resource.loader", "class");
        configuredEngine.setProperty("class.resource.loader.class",
                "org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader");
        VelocityTemplateEngine velocityTemplateEngine = new
                VelocityTemplateEngine(configuredEngine);

        //Latihan 1
        get("/hello", (req, res) -> {
            HashMap<String, Object> model = new HashMap<>();
            String templatePath = "/views/index.vm";
            return velocityTemplateEngine.render(new ModelAndView(model,
                    templatePath));
        });

        //latihan 2
        get("/overflow", (req, res)->{
            HashMap<String, Object> model = new HashMap<>();
            String someInt = req.queryParamOrDefault("someInt", "0");
            int i = Integer.parseInt(someInt);
            byte b = (byte) i;
            model.put("someInt", i);
            model.put("someByte", b);
            String templatePath = "/views/overflow.vm";
            return velocityTemplateEngine.render(new ModelAndView(model, templatePath));
        });

        //latihan 3
        get("/overflow2", (req, res)->{
            HashMap<String, Object> model = new HashMap<>();
            String someInt = req.queryParamOrDefault("someInt", "0");
            if(someInt.matches("[0-9]*")){
                int i = Integer.parseInt(someInt);

                if (i >= Byte.MIN_VALUE && i<= Byte.MAX_VALUE){
                    byte b = (byte) i;
                    model.put("someInt", i);
                    model.put("someByte", b);
                }
                else {
                    model.put("someInt", i);
                    model.put("someByte", "overflow");
                }
            }else {
                model.put("someInt", someInt);
                model.put("someByte", "maaf tidak bisa dikoversi");
            }


            String templatePath = "/views/overflow2.vm";
            return velocityTemplateEngine.render(new ModelAndView(model, templatePath));
        });

        //latihan 4
        get("/devision", (req, res)->{
            HashMap<String, Object> model = new HashMap<>();
            String aString = req.queryParamOrDefault("a", "1");
            String bString = req.queryParamOrDefault("b", "1");
            int a = Integer.parseInt(aString);
            int b = Integer.parseInt(bString);
            int c = a/b;
            model.put("a", a);
            model.put("b", b);
            model.put("c", c);
            String templatePath = "/views/devision.vm";
            return velocityTemplateEngine.render(new ModelAndView(model, templatePath));
        });

        // Latihan 5
        get("/devision2", (req, res)->{
            HashMap<String, Object> model = new HashMap<>();
            String aString = req.queryParamOrDefault("a", "1");
            String bString = req.queryParamOrDefault("b", "1");
            int a,b,c;
            if(aString.matches("[0-9]*") && bString.matches("[0-9]*")){
                a = Integer.parseInt(aString);
                b = Integer.parseInt(bString);
                model.put("a", a);
                model.put("b", b);
                if(b == 0){

                    model.put("c", "Unlimited!");
                }else {

                    c = a/b;
                    model.put("c", c);
                }

            }else {

                model.put("a", aString);
                model.put("b", bString);
                model.put("c", "bukan angka yang bisa dibagi");
            }

            String templatePath = "/views/devision2.vm";
            return velocityTemplateEngine.render(new ModelAndView(model, templatePath));
        });

        //latihan 6
        get("float", (req, res)->{
            HashMap<String, Object> model = new HashMap<>();
            String cmString = req.queryParamOrDefault("cmInput","0");
            double m = 1.0;
            double cm = 0.01;
            int cmInput = Integer.parseInt(cmString);
            double result = (m - cm * cmInput);
            model.put("m", m);
            model.put("cm", cm);
            model.put("cmInput", cmInput);
            model.put("result", result);
            String templatePath = "/views/float.vm";
            return velocityTemplateEngine.render(new ModelAndView(model, templatePath));
        });

        //latihan 7
        get("float2", (req, res)->{
            HashMap<String, Object> model = new HashMap<>();
            String cmString = req.queryParamOrDefault("cm","0");
            int m = 1000;
            int cmInput = Integer.parseInt(cmString);
            double result = (m - cmInput) / 1000.0;
            model.put("m", m);
            model.put("cmInput", cmInput);
            model.put("result", result);
            String templatePath = "/views/float2.vm";
            return velocityTemplateEngine.render(new ModelAndView(model, templatePath));
        });

        //latihan 8
        get("float3", (req, res)->{
            HashMap<String, Object> model = new HashMap<>();
            String cmString = req.queryParamOrDefault("cm","0");
            BigDecimal m = new BigDecimal("1.0");
            BigDecimal cm = new BigDecimal("0.01");
            int cmInput = Integer.parseInt(cmString);
            BigDecimal result = m.subtract(cm.multiply(new BigDecimal(cmInput)));
            model.put("m", m);
            model.put("cmInput", cmInput);
            model.put("result", result);
            String templatePath = "/views/float3.vm";
            return velocityTemplateEngine.render(new ModelAndView(model, templatePath));
        });
    }
}
